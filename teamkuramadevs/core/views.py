from django.shortcuts import render
from django.conf import settings
from django.core.mail import send_mail

# Create your views here.
def index(request):
    return render(request, "core/index.html")

def acercade(request):
    return render(request, "core/acercade.html")

def contacto(request):
    if request.method == 'POST':
        asunto = 'Coordinemos una Reunión'
        mensaje = '\nNombre: ' + request.POST['introducir_nombre'] + '\nDomicilio: ' + request.POST['introducir_domicilio'] + '\nCiudad: ' + request.POST['introducir_ciudad'] + '\nPaís: ' + request.POST['País'] + '\nCelular: ' + request.POST['introducir_telefono'] + '\nEmail: ' + request.POST['introducir_email'] + '\nMensaje: ' + request.POST['introducir_mensaje']
        email_desde = settings.EMAIL_HOST_USER
        email_para = ['canderetamozo.67@gmail.com']
        send_mail(asunto, mensaje, email_desde, email_para, fail_silently = False)
        return render(request, 'core/contactoExitoso.html')
    return render(request, "core/contacto.html")

