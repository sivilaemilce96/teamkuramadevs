from django import forms

from .models import Proyecto

class ProyectoForm(forms.ModelForm):

    class Meta:
        model = Proyecto
        fields = ['nombre', 'descripcion', 'imagen', 'url']
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Título'}),
            'descripcion': forms.Textarea(attrs={'class':'form-control'}),
            'url': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Url'}),            
        }
        labels = {
            'nombre':'', 'descripción':'', 'imagen':'', 'url':'',
        }