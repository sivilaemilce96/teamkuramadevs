from django.db import models
from ckeditor.fields import RichTextField

# Create your models here.

class Proyecto(models.Model):
    nombre = models.CharField(max_length=200, verbose_name="Nombre")
    descripcion = RichTextField(blank=True, null=True, verbose_name="Descripción")
    #descripcion = models.TextField(verbose_name="Descripción")
    imagen = models.ImageField(verbose_name="Imágen", upload_to = "proyectos")
    url = models.URLField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "proyecto"
        verbose_name_plural = "proyectos"
        ordering = ["-created"]

    def __str__(self):
        return self.nombre