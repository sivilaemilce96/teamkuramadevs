from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .forms import ProyectoForm
from .models import Proyecto

# Create your views here.

class ProyectoListView(ListView):    
    model = Proyecto
    paginate_by = 1

def buscar_proyecto(request):
    if request.method == 'POST':
        busqueda = request.POST['busqueda']
        proyects = Proyecto.objects.filter(nombre__icontains=busqueda)
        return render(request, 'proyectos/buscar_proyecto.html', {'busqueda':busqueda, 'proyects':proyects})
    else:
        return render(request, 'proyectos/buscar_proyecto.html', {}) 

class ProyectoDetailView(DetailView):
    model = Proyecto

class ProyectoCreate(CreateView):
    model = Proyecto
    #fields = ['nombre', 'descripcion', 'imagen']
    form_class = ProyectoForm
    success_url = reverse_lazy('proyectos')

class ProyectoUpdate(UpdateView):
    model = Proyecto
    #fields = ['nombre', 'descripcion', 'imagen']
    form_class = ProyectoForm
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse_lazy('proyectos')+'?Actualizado'

class ProyectoDelete(DeleteView):
    model = Proyecto
    def get_success_url(self):
        return reverse_lazy('proyectos')+'?Eliminado'