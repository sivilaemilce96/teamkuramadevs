"""teamkuramadevs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from core import views
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from proyectos import views as proyectos_views
from proyectos.views import (ProyectoCreate, ProyectoDelete, ProyectoListView, ProyectoDetailView, ProyectoUpdate)

urlpatterns = [
    path('', views.index,name='index'),
    path ('acercade/', views.acercade,name='acercade'),
    path ('contacto/', views.contacto,name='contacto'),
    path('trabajos_realizados', ProyectoListView.as_view(),name='proyectos'),
    path('proyecto/<int:pk>/', ProyectoDetailView.as_view(), name='proyecto-detail'),
    path('create/', ProyectoCreate.as_view(), name='create'),
    path('update/<int:pk>/', ProyectoUpdate.as_view(), name='update'),		
    path('delete/<int:pk>/', ProyectoDelete.as_view(), name='delete'),
    path('miembros/', include('django.contrib.auth.urls')),
    path('miembros/', include('miembros.urls')),
    path('buscar_proyecto', proyectos_views.buscar_proyecto, name='buscar-proyecto'),
    path('admin/', admin.site.urls),
]

# Configuración extendida para mostrar las imágenes en modo debug

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)